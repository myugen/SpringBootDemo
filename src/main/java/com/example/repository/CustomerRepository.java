package com.example.repository;

import com.example.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by a629213 on 02/08/2017.
 */

public interface CustomerRepository extends JpaRepository<Customer, Long> {

    List<Customer> findAll();
    List<Customer> findByLastName(@Param("lastName") String lastName);
    List<Customer> findByFirstNameContaining(@Param("firstName") String firstName);
}

